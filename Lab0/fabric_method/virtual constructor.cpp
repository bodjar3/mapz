#include "Animal.h"
#include "Cow.h"
#include "Pig.h"
#include "Chicken.h"
#include "assert.h"
#include <vector>
using namespace std;
Animal::Animal(Animal_Id id)
{
	if (id == Cow_Id) p = new Cow;
	else if (id == Pig_Id) p = new Pig;
	else if (id == Chicken_Id) p = new Chicken;
	else assert(false);
}
int main()   
{
	vector <Animal*> v;
	v.push_back(new Animal(Cow_Id));
	v.push_back(new Animal(Pig_Id));
	v.push_back(new Animal(Chicken_Id));
	for ( int i = 0; i < v.size(); i++)
	{
		v[i]->OutputSound();
	}
}

// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
