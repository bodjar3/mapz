#pragma once
#include <iostream>
enum Animal_Id {Cow_Id=0,Pig_Id,Chicken_Id};
class Animal
{
public :
	Animal() { p = 0; }
	Animal(Animal_Id id);
	virtual void OutputSound() { p->OutputSound(); }
	virtual ~Animal() { delete p; p = 0;}
private:
	Animal *p;
};

