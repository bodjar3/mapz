#pragma once
#include "Animal.h"
class Chicken : public Animal
{
public:
	void OutputSound() {
		std::cout << "Bwak...BWAAAAA\n";
	}
private:
	Chicken() : Animal() {}
	Chicken(Chicken&);
	Chicken operator=(Chicken&);
	friend class Animal;
};

