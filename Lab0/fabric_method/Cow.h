#pragma once
#include "Animal.h"
class Cow : public Animal
{
public :
	void OutputSound() {
		std::cout << "Mou...MOUUUUUU\n";
	}
private : 
	Cow() : Animal() {}
	Cow(Cow&);
	Cow operator=(Cow&);
	friend class Animal;
};

