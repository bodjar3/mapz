#pragma once
#include "Animal.h"
class Pig : public Animal
{
public:
	void OutputSound() {
		std::cout << "Chromk...CHROOOO\n";
	}
private:
	Pig() : Animal() {}
	Pig(Pig&);
	Pig operator=(Pig&);
	friend class Animal;
};

