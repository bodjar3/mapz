// warrior type.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <vector>

using namespace std;
enum Warrior_ID { Infantryman_ID, Archer_ID, Horseman_ID };

class Warrior
{
public:
	Warrior() : p(0) { }
	Warrior(Warrior_ID id);
	virtual void info() { p->info(); }
	virtual ~Warrior() { delete p;  p = 0; }
private:
	Warrior* p;
};

class Infantryman : public Warrior
{
public:
	void info() { cout << "Infantryman" << endl; }
private:
	Infantryman() : Warrior() {}
	Infantryman(Infantryman&);
	Infantryman operator=(Infantryman&);
	friend class Warrior;
};

class Archer : public Warrior
{
public:
	void info() { cout << "Archer" << endl; }
private:
	Archer() : Warrior() {}
	Archer(Archer&);
	Archer operator=(Archer&);
	friend class Warrior;
};

class Horseman : public Warrior
{
public:
	void info() { cout << "Horseman" << endl; }
private:
	Horseman() : Warrior() {}
	Horseman(Horseman&);
	Horseman operator=(Horseman&);
	friend class Warrior;
};

Warrior::Warrior(Warrior_ID id)
{
	if (id == Infantryman_ID) p = new Infantryman;
	else if (id == Archer_ID) p = new Archer;
	else if (id == Horseman_ID) p = new Horseman;
	else cout << "bad type of warrior";
}


int main()
{
	vector<Warrior*> v;
	v.push_back(new Warrior(Infantryman_ID));
	v.push_back(new Warrior(Archer_ID));
	v.push_back(new Warrior(Horseman_ID));

	for (int i = 0; i < v.size(); i++)
		v[i]->info();
	
}



// Run program: Ctrl + F5 or Debug > Start Without Debugging menu
// Debug program: F5 or Debug > Start Debugging menu

// Tips for Getting Started: 
//   1. Use the Solution Explorer window to add/manage files
//   2. Use the Team Explorer window to connect to source control
//   3. Use the Output window to see build output and other messages
//   4. Use the Error List window to view errors
//   5. Go to Project > Add New Item to create new code files, or Project > Add Existing Item to add existing code files to the project
//   6. In the future, to open this project again, go to File > Open > Project and select the .sln file
